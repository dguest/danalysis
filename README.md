More-Better AthAnalysis Image
===============================

The default AthAnalysis image is missing a lot of stuff that I
like. This is an attempt to customize it a bit.

Unfortunately this will only work if your username is `dguest`: quite
a lot is hardcoded to the username. So you can use this for
inspiration, but probably not out of the box.


Running
=======

You can pull the latest image and build it with

```
build-docker.sh
```

and then run it with

```
run-docker.sh
```

this should work from anywhere on your system, you don't need to run
from within the source directory. You will be dropped into a container
based on the `work` directory under this one though.

SSH Keys
--------

By default the run script will pull your keys from `~/.ssh/gitlab` or
(if no such key exists) `~/.ssh/id_rsa`. If you want to generate a new
key to use just for this container:

 - Open the [gitlab key entry forum][1]
 - run `make-key.sh | pbcopy` and paste into the box there

Building
========

You can build your own version by running `build-docker.sh` from
within this repository.

[1]: https://gitlab.cern.ch/-/user_settings/ssh_keys
