ARG TAG
FROM gitlab-registry.cern.ch/atlas/athena/athanalysis:25.2.${TAG}

USER root
RUN \
    pushd /tmp/ && \
    wget https://repo.ius.io/ius-release-el7.rpm && \
    rpm -i --nodeps ius-release-el7.rpm && \
    popd && \
    yum install -y jq git-lfs tree bash-completion && \
    yum clean all && \
    true

# python installs
WORKDIR /home/atlas
RUN \
  . /home/atlas/release_setup.sh && \
  true

COPY custom-rc.bash /home/atlas/.bashrc

COPY _h5ls/_h5ls.sh /etc/dguest/
COPY git-fatlas/git-fatlas.sh /etc/dguest
COPY pandamonium/pandamon /usr/local/bin/
COPY mark-and-jump/mark-and-jump.sh /etc/dguest
COPY get-branches /usr/local/bin
COPY git-tools/git-wrap /usr/local/bin/

USER atlas
WORKDIR /home/atlas/work

CMD /bin/bash
