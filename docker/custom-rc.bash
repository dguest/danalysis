# bashrc file for the images

# set up command line
CLICOLOR=1

# add dan's utils
for SCRIPT in /etc/dguest/*; do
    source $SCRIPT
done
unset SCRIPT

# enable tab complete
source /etc/profile.d/bash_completion.sh
source /home/atlas/release_setup.sh > /dev/null

cp /home/atlas/settings/git/config ~/.gitconfig
cp -r /home/atlas/settings/ssh ~/.ssh

# set up the command line
PS1='\[\033[01;31m\]AthAnalysis-$AthAnalysis_VERSION:\[\033[33m\]\t:\[\033[01;32m\]:\w>\[\033[00m\] ';

HISTFILE=${HOME}/work/.bash_history
HISTFILESIZE=""
HISTSIZE=""
DIRECTORY_STACK_FILE=${HOME}/work/.WORKING_DIRECTORY_STACK
load-dir-stack

# aliases
alias ls='ls --color=auto'

# HDF5 has file locking issues on my docker images for some reason
export HDF5_USE_FILE_LOCKING=FALSE

# save the directory stack when we leave
trap save-dir-stack EXIT
